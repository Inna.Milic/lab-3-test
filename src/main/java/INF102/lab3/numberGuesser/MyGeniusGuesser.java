package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {
    private int lowerbound;
    private int upperbound;

	@Override
    public int findNumber(RandomNumber number) {
        lowerbound = number.getLowerbound();
        upperbound = number.getUpperbound();

        while (upperbound > lowerbound) {

            int middle = (upperbound - lowerbound) + lowerbound / 2;
            int guess = number.guess(middle);

            if (makeGuess(middle, guess)) {
                return middle;
            }
        }
        return upperbound;
    }

    private boolean makeGuess(int middle, int guess) {
        if( guess == -1){
            lowerbound = middle + 1;
        }
        else if(guess == 1){
            upperbound = middle -1;
        }
        else{
            return true;
        }
        return false;
    }

}
