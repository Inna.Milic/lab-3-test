package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        if (numbers.isEmpty()) {
            throw new IllegalArgumentException("The list is empty");
        }
        if(numbers.size() == 1){
            return numbers.get(0);
        }
        return numbers.get(binarySearch(numbers, 0, numbers.size() - 1));
    }

    public int binarySearch(List<Integer> numbers,int left,int right) {
        if (right == left) {
            return left;
        }
        int middle = (right + left) / 2;

        if (numbers.get(middle) > numbers.get(middle + 1)) {
            return binarySearch(numbers, left, middle);
        }
        return binarySearch(numbers, middle + 1, right);
    }

}
